#include "cmdparser.h"

void cmdparser::parse(const char* in){
  string c(in);
  cout << c << endl;
  vector<string> parts = split(c);
  printvect(parts);
  //parts[0]; track-path
  //parts[1]; command
  //parts[2]; filename
  string filename = v.at(0); //path current
  setFilename(filename, v.at(2));
  cout << "filename: " <<  filename << endl;

  if(parts.size()>2 && !fop->is_dir(filename) && parts.at(2).compare("ALL")!=0)
  {cout<<"Directory names only, cannot track single files with current version..."<<endl; return;}

  if(parts.at(1).compare("stop") == 0){
    cout<< "got stop parse" << endl;
    track->stopwatch();
  }
  else if(isTrackCMD(parts)){
    trackParse(parts);
  }

  else if(isUnTrackCMD(parts)){
    untrackParse(parts);
  }
  else if(isshowcmd(parts)){
    show(parts.at(0));
  }

  else
    {cout << "invalid command, refer to help.txt for assistance" << endl;}


}

void cmdparser::trackParse(const vector<string> & v){
  // path track "file"
  int args = v.size();
  printvect(v);
        //0     1            2       3
  // ver path track "file name ok" changes
  if(args == 3 && v.at(1).compare("track") == 0){
    string filename = v.at(0); //path current
    setFilename(filename, v.at(2));
    cout<< "Filename interperetation: "<<filename<<endl;

    cout << "file absolute path:" << filename << endl;
    track->addtrack(&filename);
  }
  else if(args == 4 && v.at(1).compare("track") == 0 && v.at(3).compare("-r")==0){
    string filename = v.at(0); //path current
    setFilename(filename, v.at(2));
    cout<< "-r Filename interperetation: "<<filename<<endl;

    cout << "file absolute path:" << filename << endl;
    track->setRecursiveTrack(true,filename);
    track->addtrack(&filename);

  }
  else{

    cout << "didn't parse" << "\n";
  }

}

void cmdparser::untrackParse(const vector<string> & v){
  // path track "file"
  int args = v.size();
  cout << "in untrackparse" << endl;
  printvect(v);

  if(args == 3 && v.at(1).compare("untrack") == 0){
    string filename = v.at(0); //path current
    setFilename(filename, v.at(2));
    cout << "file absolute path:" << filename << endl;
    track->removetrackfile(&filename);
  }
  else if(args == 4 && v.at(1).compare("untrack") == 0 && v.at(3).compare("-r")==0){
    string filename = v.at(0); //path current
    setFilename(filename, v.at(2));
    cout << "file absolute path:" << filename << endl;
    track->setRecursiveUntrack(true);
    track->removetrackfile(&filename);

  }
  else{

    cout << "didn't parse" << "\n";
  }

}


bool cmdparser::isUnTrackCMD(const vector<string> & v){
  if(v.size() >= 2 && v.at(1).compare("untrack") == 0){
      return true;
  }
  return false;
}

/* both track and untrack file and dirs */
bool cmdparser::isTrackCMD(const vector<string> & v){
  if(v.size() >= 2 && v.at(1).compare("track") == 0){
      return true;
  }
  return false;
}

cmdparser::cmdparser(){
  track = new Track();
  fop = new fileoperations();
  //track->start_event_watcher_thread();
}

cmdparser::~cmdparser(){
  free(track);
  free(fop);
}

void cmdparser::printvect(const vector<string> & v){
  for (vector<string>::const_iterator it = v.begin() ; it != v.end(); ++it)
  		cout << *it << endl;
}

vector<string> cmdparser::split(const string & s){
  vector<string> data;
  bool ss = false; //start special '#'
  bool se = false; // end special '#'

  string part = "";
  for(int i = 0; i < s.length(); i++){
    if(s[i] == '#' && ss == false && se == false){
      ss = true;
    }
    else if(s[i] == '#' && ss == true && se == false){
      se = true;
    }

    if(s[i] != '#'){
      part = part.append(1, s[i]);
    }

    if(ss && se){
      ss = false;
      se = false;
      data.push_back(part);
      part = "";
    }
  }
  return data;
}

void cmdparser::setFilename(string & filename, const string & userPath){
  if(userPath.compare("./")==0)
    {filename;}

  else if(userPath.substr(0,2).compare("./")==0)
    {filename.append(userPath.substr(1));}

  else if(userPath.substr(0,1).compare("/")==0)
    {filename = userPath;}

  else
    {filename.append("/");
    filename.append(userPath);}

}

bool cmdparser::isshowcmd(const vector<string> & v){
  if(v.size() == 2 && v.at(1).compare("show") == 0){
    cout << "is show cmd" << endl;
    return true;
  }
  return false;
}

//show
string cmdparser::show(const string & in){
  string out = "";
string showExec = "./showClient";

  //check if dirs in path are in track container
  cout << "printing tracked in current path:\n";
  string userpath = in;
  if (auto dir = opendir(in.c_str())) {
    while (auto f = readdir(dir)) {
        if (!f->d_name || f->d_name[0] == '.'){
          continue;
        }
        if(f->d_type == DT_DIR){
          string d(f->d_name);
          string path = (in+"/"+d);
          string trackPath = "tracking_container/"+track->directify(path);

          auto check = opendir(trackPath.c_str());



          //cout <<"DIR:" << d << endl;

          string incontname = in + '/' + d;
          string incontnamenospec = incontname;
          replace(incontname.begin(), incontname.end(), '/', '^');
          //cout << "made:" << incontname << endl;
          if(fop->is_dir(incontnamenospec.c_str()) && check){
            cout << "tracked dir: " << d << endl;
            closedir(check);
          }

        }
    }
  }

  //print out tracked filed from tracked container for current path
  string path = in;
  //cout<< "in show cmd path:" << path << endl;
  replace(path.begin(), path.end(), '/', '^');

  string trackcontpath =  TRACKING_CONTAINER_PATH;
  string htemp = trackcontpath + '/' + path;
  //cout << "opening:" << htemp << endl;
  if (auto dir = opendir(htemp.c_str())) {
    while (auto f = readdir(dir)) {
        if (!f->d_name || f->d_name[0] == '.'){
          continue;
        }
          if(f->d_type == DT_REG){
          string d(f->d_name);
          cout <<"tracked file:" << d << endl;
          if(d.compare("trackList.txt") != 0){
            replace( d.begin(), d.end(), '^', '/');
            string temp = trackcontpath;
            temp += '/' + d;
            d = temp;

          }
        }
      }
  }
  cout<<"end of tracked for current path"<<endl;
  //cout << "in path:" << in << endl;
  return out;
}
/* return true if input file is on the same level as the path*/
bool cmdparser::isoneleveldeep(const string & prefix, const string & abs){

  //cout << "in:" << abs << endl;
  string result = abs;
  int plen = prefix.length();

  string substr = abs.substr(0, plen);
  string after = abs.substr(plen, abs.length());
  //cout << "substr:" << substr << endl;
  //cout << "after:" << after << endl;

  if(after.length() > 0){
    if(after[0] == '/'){
        int i = 1;
        while(i < abs.length() && after[i] != '/'){
          i++;
        }
        i--;
        //cout << "index:" << i << endl;
        string nm = after.substr(1, i);
        string origin = abs.substr(plen, abs.length());
        //cout << "abs:" << abs << endl;
        //cout << "original:" << origin << endl;
        //cout << "second word:" << nm << endl;

        int times = count(origin.begin(), origin.end(), '/');
        //cout << "times:" << times << endl;
        if(times > 1){
          //cout << "false" << endl;
          return false;
        }
        else{
          if(nm.length() == 0){
            //cout << "false" << endl;
            return false;
          }
          else{
            //cout << "true" << endl;
            return true;
          }
        }
    }
  }
  cout << "false" << endl;
  return false;
}




//Run on server.c startup, to populate the track object
void cmdparser::initTrack(){
  //track->startnotify();
}
