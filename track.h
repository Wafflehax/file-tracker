#include <unordered_map>
#include <map>
#include <unistd.h>
#include <sys/socket.h>
#include <stdio.h>
#include <cstdlib>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <limits.h>
#include <string.h>
#include <sys/un.h>
#include <string>
#include <iostream>
#include <unordered_set>
#include <vector>
#include <pthread.h>
#include <thread>
#include <signal.h>
#include <atomic>
#include <fstream>
#include <algorithm>
#include <dirent.h>
#include "PathConfig.h"
#include "connection.h"
using namespace std;

#define MAX_EVENTS 1024 /*Max. number of events to process at one go*/
#define LEN_NAME 16 /*Assuming that the length of the filename won't exceed 16 bytes*/
#define EVENT_SIZE  ( sizeof (struct inotify_event) ) /*size of one event*/
#define BUF_LEN     ( MAX_EVENTS * ( EVENT_SIZE + LEN_NAME )) /*buffer to store the data of events*/


class Track{

private:
  unordered_set<string> tracks; //all items which are watched for changes
  map<int, string> wd_paths;
  string recWatch;
  string trackPath;
  string recoverPath;
  bool inRemove;
  bool recursiveTrack = false;
  bool recursiveUntrack = false;


  volatile sig_atomic_t term = false;
  volatile sig_atomic_t running = false;

  //char* buffer = (char*) calloc(BUF_LEN, sizeof(char));

  pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER; // for tracks access
  pthread_t t;

  int fd;
  void start_event_watcher_thread();

public:
//
  void shutdownwatchthread();
  void stopwatch();
  void startnotify();
//  void addWatch(const char* s);
  void starteventloop();
  bool isFile(const string* filename);
  void initnotify();
  void populateTrackList();

  void addToTrackList(const string & filename);
  void removeFromTrackList(const string & filename);
  string directify(const string & dirName);
  string undirectify(const string & dirName);
  void subDirectoriesAdd(const string & name);
  void subDirectoriesRemove(const string & name);
  bool endsWith (std::string const &fullString, std::string const &ending);
  void setDoTrack(bool set);
  void setDoUntrack(bool set);
  void setRecursiveTrack(bool set, const string & rec);
  void setRecursiveUntrack(bool set);
// above three pirvate ?
  Track();
  ~Track();

  void addtrack(const string* path); //s filename, p file path master method
  void removetrackfile(const string* path);

};
