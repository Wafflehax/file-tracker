#include "fileoperations.h"

bool fileoperations::is_file(const char* path) {
    struct stat buf;
    stat(path, &buf);
    return S_ISREG(buf.st_mode);
}

bool fileoperations::is_dir(const char* path) {
    struct stat buf;
    stat(path, &buf);
    return S_ISDIR(buf.st_mode);
}


fileoperations::fileoperations(){

}

fileoperations::~fileoperations(){

}
