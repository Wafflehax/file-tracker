To ensure a complete installation... untar the provided tar file in whichever working directory you so choose.

1) Go to your home directory (ie. $cd)
->$vi .bashrc
->at the bottom of the file, add the line "PATH=$PATH:.xxx/file-snapshot-tool/scripts" :: (where xxx is your prefix to this path)

2) Go to your xxx/file-snapshot-tool/scripts file, then edit the 'ver' script :: (where xxx is your prefix to this path)
->change the line 'cd /home/student/Desktop/file-snapshot-tool' to 'xxx/file-snapshot-tool' :: (where xxx is your prefix to this path)

3) Go to your xxx/file-snapshot-tool/PathConfig.h file
-> Change "/home/student/Desktop/file-snapshot-tool/tracking_container" to "xxx/file-snapshot-tool/tracking_container" :: (where xxx is your prefix to this path)
-> Change "/home/student/Desktop/file-snapshot-tool/recoverClient" to "xxx/file-snapshot-tool/recoverClient" :: (where xxx is your prefix to this path)

NOTE: Ensure that you have installed the program 'lxterminal'... It is essential for offering updates
$sudo apt-get install lxterminal

4) Now you should be good to compile the program.  Navigate to your xxx/file-snapshot-tool, and execute...
$make

5) In a seperate terminal, run the server.

6) From any other terminal, you can now use the $ver track directory, $ver untrack directory, $ver track directory -r (recursive track), $ver untrack directory -r (recursive untrack).
