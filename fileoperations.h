#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H

#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <string.h>

#include <stdio.h>
#include <iostream>

using namespace std;

class fileoperations{

public:
fileoperations();
~fileoperations();

bool is_dir(const char* path);
bool is_file(const char* path);


};
#endif
