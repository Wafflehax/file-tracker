#include "track.h"

Track::Track(){

  //starteventloop();
    inRemove = false;
    populateTrackList();
    start_event_watcher_thread();
    const char * tptemp = TRACKING_CONTAINER_PATH;
    const char * rptemp = RECOVER_CLIENT_PATH;
    trackPath = string(tptemp);
    recoverPath = string(rptemp);
}

Track::~Track(){

}

void Track::shutdownwatchthread(){
  term = true;
  close(fd);
}

void Track::addtrack(const string* path){
  string ignore = trackPath;
  string p = *path;

  if(p.compare(0,ignore.length(),ignore)==0)
  {cout<<"Can't track anything within the tracking container"<<endl; return;}

  auto search = tracks.find(p);

      tracks.insert(p);
      cout << "inserted\n";
      //stop and restart watch loop

      stopwatch();
      start_event_watcher_thread();


    }


void Track::removetrackfile(const string* path){
  string ignore = trackPath;
  string p = *path;
  cout<<"In untrack: "<< p <<endl;

  if(p.compare("ALL")==0){string untrackALL = "tracking_container/*";
    tracks.clear();
    string execute = "exec rm -r ";
    system((execute +  untrackALL).c_str());
    return;
  }

  if(p.compare(0,ignore.length(),ignore)==0)
  {cout<<"Can't untrack anything within the tracking container"<<endl; return;}

  if(recursiveUntrack)
  {subDirectoriesRemove(p);}
  recursiveUntrack=false;

  string execute = "exec rm -r ";
      string untrackDir = "tracking_container/"+directify(p);
      cout << untrackDir << endl;
    removeFromTrackList(p);
    tracks.erase(p);
    system((execute +  untrackDir).c_str());


      stopwatch();
      start_event_watcher_thread();


    }


void Track::start_event_watcher_thread()
{
  term = false;
	cout << "Launching event loop watcher thread\n";
  thread t([&]{
    starteventloop();
  });
	t.detach(); // do not wait for the thread to finish
}

void Track::stopwatch(){
  term = true;
  close( fd );
  wd_paths.clear();
}

void Track::starteventloop(){
 /* do it forever*/
  int length;
  int i;
  int wd;
  char buffer[BUF_LEN];

  fd = inotify_init();
  if ( fd < 0 ) {
    perror( "Couldn't initialize inotify");
  }

  for (std::unordered_set<string>::iterator itr = tracks.begin(); itr != tracks.end(); ++itr) {
    string s = *itr;
    /*IN_MOVE_SELF
    Watched file/directory was itself moved
    */
    wd = inotify_add_watch(fd, s.c_str(), IN_CREATE | IN_MODIFY | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF);
    if (wd == -1)
    {
      printf("Couldn't add watch to %s\n", s.c_str());
    }
    else
    {
      printf("added to inotify watch %s\n", s.c_str());

      wd_paths.insert(pair<int,string>(wd,s));

//THIS IS WHERE THE SUBDIRECTORIES ARE ADDED

      if(recursiveTrack && s.compare(recWatch)==0) subDirectoriesAdd(s);
      addToTrackList(s);
      recWatch = "";

      //copy directory over
      //cout << "debugging directify"<<endl;
      string newDir = "tracking_container/"+directify(s);

      mkdir(newDir.c_str(),S_IRUSR | S_IWUSR | S_IXUSR);
    }
  }

 while(term == false){
     i = 0;
     length = read(fd, buffer, BUF_LEN);


     if ( length < 0 ) {
       perror( "read" );
     }
     while (term == false && i < length ) {
       struct inotify_event *event = ( struct inotify_event * ) &buffer[ i ];
       if ( event->len ) {
         if ( event->mask & IN_CREATE) {
           if (event->mask & IN_ISDIR)
               {
                  //ADD NEW DIRECTORY TO THE ROTATION
                 printf( "\n\n~~~~The directory %s was Created~~~~\n", event->name );
               string temp(event->name);
               string dirName = wd_paths.find(event->wd)->second + "/" + temp;
               cout<<"dirname = " + dirName<<endl;
               wd = inotify_add_watch(fd, dirName.c_str(), IN_CREATE | IN_MODIFY | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF);
               if (wd == -1)
               {
                 printf("Couldn't add watch to %s\n", dirName.c_str());
               }
               else
               {
                 printf("added to inotify watch %s\n", dirName.c_str());

                 tracks.insert(dirName);
                 wd_paths.insert(pair<int,string>(wd,dirName));

                 //copy directory over
                 string newDir = "tracking_container/"+directify(dirName);
                 mkdir(newDir.c_str(),S_IRUSR | S_IWUSR | S_IXUSR);

               }


           }
           else
           {string temp(event->name);
              if(temp.compare("4913")==0)
                  {}

              else if(endsWith(temp,".swp") || endsWith(temp,".swpx") || endsWith(temp,"~") || endsWith(temp, ".swx"))
                {}

                else{ printf( "The file %s was created with WD %d\n", (wd_paths.find(event->wd)->second + "/" + temp).c_str(), event->wd );}
            }
         }

         if ( event->mask & IN_MODIFY) {
           if (event->mask & IN_ISDIR)
             printf( "The directory %s was modified.\n", event->name );
           else
             {string temp(event->name);


               if(temp.compare("4913")==0)
                   {}

               else if(endsWith(temp,".swp") || endsWith(temp,".swpx"))
                 {}
            else
              {printf( "The file %s was modified with WD %d\n", (wd_paths.find(event->wd)->second + "/" + temp).c_str(), event->wd );
                ifstream fromUser(wd_paths.find(event->wd)->second + "/" + temp, ios::binary);

                if(fromUser)
                {//GOOD TO COPY
                  ofstream intoTrack("tracking_container/"+directify(wd_paths.find(event->wd)->second)+"/"+temp, ios::binary);
                  if(intoTrack) intoTrack << fromUser.rdbuf();
                }

                else
                  {printf("ERROR TRYING TO COPY TO TRACKING DIRECTORY\n");}

              }
           }
         }

         if ( event->mask & IN_DELETE) {

           if (event->mask & IN_ISDIR)
             printf( "The directory %s was deleted.\n", event->name );
           else
           {
             string temp(event->name);

             if(temp.compare("4913")==0)
                 {}


             else if(endsWith(temp,".swp") || endsWith(temp,".swpx") || endsWith(temp,"~") || endsWith(temp, ".swx") || endsWith(temp, ".swx"))
               {}

             else {

                  printf("The file %s was deleted with WD %d\n", (wd_paths.find(event->wd)->second + "/" + temp).c_str(), event->wd );

                  string filename(wd_paths.find(event->wd)->second + "/" + temp);

                  string executionPrefix("lxterminal -e \"bash -c \'"+recoverPath+" ");

                  string executionSuffix("\'; bash\"");
                  string socketKey(" tSocket5460");

                  while(inRemove)
                  {sleep(3);}
                  inRemove = true;

                  int ret;
                  int connection_socket;
                  int data_socket;
                  struct sockaddr_un name;
                  unlink(SOCKET_NAME_2);

                  connection_socket = socket(AF_UNIX, SOCK_SEQPACKET, 0);
                  if (connection_socket == -1) {
                      perror("socket");
                      exit(EXIT_FAILURE);
                  }
                  /*
                   * For portability clear the whole structure, since some
                   * implementations have additional (nonstandard) fields in
                   * the structure.
                   */
                  memset(&name, 0, sizeof(struct sockaddr_un));
                  /* Bind socket to socket name. */
                  name.sun_family = AF_UNIX;
                  strncpy(name.sun_path, SOCKET_NAME_2, sizeof(name.sun_path) - 1);
                  ret = bind(connection_socket, (const struct sockaddr *) &name,
                             sizeof(struct sockaddr_un));
                  if (ret == -1) {
                      perror("bind");
                      exit(EXIT_FAILURE);
                  }

                  /*
                   * Prepare for accepting connections. The backlog size is set
                   * to 20. So while one request is being processed other requests
                   * can be waiting.
                   */

                  ret = listen(connection_socket, 20);
                  if (ret == -1) {
                      perror("listen");
                      exit(EXIT_FAILURE);
                  }

                  char buffer[BUFFER_SIZE];
                  system((executionPrefix + filename + socketKey + executionSuffix).c_str());
                  data_socket = accept(connection_socket, NULL, NULL);



                  if (data_socket == -1) {
                      perror("accept");
                      exit(EXIT_FAILURE);
                    }

                     /* Wait for next data packet. */
                     //clear
                     buffer[0] = '\0';
                     ret = read(data_socket, buffer, BUFFER_SIZE);
                     if (ret == -1) {
                       perror("read");
                       exit(EXIT_FAILURE);
                     }
                     else
                    { if(strncmp(buffer, "yes\0", 5)==0)
                        {ifstream fromTrack("tracking_container/"+directify(wd_paths.find(event->wd)->second)+"/"+temp, ios::binary);

                      if(fromTrack)
                        {ofstream intoUser(wd_paths.find(event->wd)->second + "/" + temp, ios::binary);
                        if(intoUser) intoUser << fromTrack.rdbuf();
                        }
                      }

                    }
                    strncpy(buffer, "WORK HAS BEEN DONE", BUFFER_SIZE);
                    ret = write(data_socket, buffer, BUFFER_SIZE);

                    if (ret == -1) {
                        perror("write");
                        exit(EXIT_FAILURE);
                    }
                    /* Close socket. */
                    unlink(SOCKET_NAME_2);
                    close(data_socket);
                    inRemove = false;
                }
            }
         }

         if( event->mask & IN_MOVE_SELF){
           if (event->mask & IN_ISDIR)
             printf( "The directory %s was moved.\n", event->name );
           else
             printf( "The file %s was moved with WD %d\n", event->name, event->wd );
         }

         i += EVENT_SIZE + event->len;
       }
     }
   }
  cout<< "watch event loop stopped" << endl;
  /* Clean up*/
  inotify_rm_watch( fd, wd );
  close( fd );
}



//Not Tested
void Track::populateTrackList(){
ifstream tList;
string next;
vector<string> listOut;
    tList.open("./tracking_container/trackList.txt");
    if (!tList) {printf("TrackList Created\n");}

    while (getline(tList,next)) {tracks.insert(next);}
tList.close();

}

void Track::addToTrackList(const string & filename){
  /*ofstream tList;
  tList.open("./tracking_container/trackList.txt",ios_base::app);
  tList << filename << endl;
  tList.close();*/

  ifstream tList;
  ofstream temp;
  string line;
    tList.open("./tracking_container/trackList.txt");
    temp.open("./tracking_container/tempList.txt");

    while (getline(tList,line))
    {
        if (filename.compare(line)!=0)
        {
            temp << line << endl;
        }
    }
    temp << filename << endl;
    temp.close();
    tList.close();
    remove("./tracking_container/trackList.txt");
    rename("./tracking_container/tempList.txt","./tracking_container/trackList.txt");
}

void Track::removeFromTrackList(const string & filename){
  ifstream tList;
  ofstream temp;
  string line;
    tList.open("./tracking_container/trackList.txt");
    temp.open("./tracking_container/tempList.txt");

    while (getline(tList,line))
    {
        if (filename.compare(line)!=0)
        {
            temp << line << endl;
        }
    }
    temp.close();
    tList.close();
    remove("./tracking_container/trackList.txt");
    rename("./tracking_container/tempList.txt","./tracking_container/trackList.txt");
}

string Track::directify(const string & dirName){
  string out = dirName;
replace( out.begin(), out.end(), '/', '^');
return out;
}

string Track::undirectify(const string & dirName){
  string out = dirName;
replace( out.begin(), out.end(), '^', '/');
return out;
}

void Track::subDirectoriesAdd(const string & path)
{int wd;
  string ignore = trackPath;
  int children = 0;
    if (auto dir = opendir(path.c_str())) {
        while (auto f = readdir(dir))
        {children++;
            if (f->d_name[0] == '.'){ continue;}

            if (f->d_type == DT_DIR && path.compare(0, ignore.length(),ignore)!=0)
                {//ADDING OCCURS HERE
                  wd = inotify_add_watch(fd, path.c_str(), IN_CREATE | IN_MODIFY | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF);

                  if (wd == -1)
                  {
                    printf("Couldn't add watch to %s\n", path.c_str());
                  }
                  else
                  {
                    printf("added to inotify watch %s\n", path.c_str());

                    wd_paths.insert(pair<int,string>(wd,path));
              //THIS IS WHERE THE SUBDIRECTORIES ARE ADDED
                    //subDirectoriesAdd(s);
                    addToTrackList(path);

                    //copy directory over
                    string newDir = "tracking_container/"+directify(path);
                    mkdir(newDir.c_str(),S_IRUSR | S_IWUSR | S_IXUSR);
                  }


                  subDirectoriesAdd(path + "/" + f->d_name);
                }


        }
        closedir(dir);
    }
    if(children < 3)
    {wd = inotify_add_watch(fd, path.c_str(), IN_CREATE | IN_MODIFY | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF);

    if (wd == -1)
    {
      printf("Couldn't add watch to %s\n", path.c_str());
    }
    else
    {
      printf("added to inotify watch %s\n", path.c_str());

      wd_paths.insert(pair<int,string>(wd,path));
//THIS IS WHERE THE SUBDIRECTORIES ARE ADDED
      //subDirectoriesAdd(s);
      addToTrackList(path);

      //copy directory over

      string newDir = "tracking_container/"+directify(path);

      mkdir(newDir.c_str(),S_IRUSR | S_IWUSR | S_IXUSR);
    }}
}

void Track::subDirectoriesRemove(const string & path)
{string ignore = trackPath;
  int children = 0;
    if (auto dir = opendir(path.c_str())) {
        while (auto f = readdir(dir)) {children++;
            if (f->d_name[0] == '.') continue;


            if (f->d_type == DT_DIR && path.compare(0, ignore.length(),ignore)!=0)
                {   string execute = "exec rm -r ";
                    string untrackDir = "tracking_container/"+directify(path);


                    removeFromTrackList(path);
                    tracks.erase(path);

                    system((execute +  untrackDir).c_str());
                  }


                  subDirectoriesRemove(path + "/" + f->d_name);
                }

closedir(dir);
        }


    if(children < 3)
    { string execute = "exec rm -r ";
        string untrackDir = "tracking_container/"+directify(path);
      removeFromTrackList(path);
      tracks.erase(path);

      system((execute +  untrackDir).c_str());
    }
}

void Track::setRecursiveTrack(bool set, const string & rec) {recursiveTrack = set; recWatch = rec;}
void Track::setRecursiveUntrack(bool set) {recursiveUntrack = set;}

bool Track::endsWith (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

//MAIN USED TO TEST TRACK
/*int main(){
Track *track = new Track();
track->addToTrackList("fuck.png");
track->addToTrackList("fuck2.png");
track->addToTrackList("fuckingPUSS.png");

vector<string> theList = track->getTrackList();

for(int i=0; i<theList.size(); i++)
{printf("theList[%d]:\t[%s]\n",i,theList[i].c_str());}

track->removeFromTrackList("fuck.png");
track->subDirectoryGet(".",0);


}*/
