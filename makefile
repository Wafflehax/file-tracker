OBJCMDPARSERFILES =  track.o cmdparser.o fileoperations.o

all: server.o client.o $(OBJCMDPARSERFILES)
	g++ -o server -pthread server.o $(OBJCMDPARSERFILES)
	g++ -o client client.o
	g++ -o recoverClient recoverClient.c

fileoperations.o:
	g++ -c -std=c++11 fileoperations.h fileoperations.c
track.o: track.c
	g++ -c -pthread -std=c++11 track.h track.c
cmdparser.o: cmdparser.c
	g++ -c -std=c++11 cmdparser.h cmdparser.c
server.o: server.c
	g++ -c -pthread -std=c++11 server.c
client.o: client.c
	g++ -c client.c

recoverClient.o: recoverClient.c
	g++ -c recoverClient.c

clean:
	rm -f *.o *~ core *~ server client cmdparser recoverClient *.h.gch
