#ifndef CMD_PARSER_H
#define CMD_PARSER_H
#include "track.h"
#include <stdlib.h>
#include <string.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "fileoperations.h"
#include <regex.h>

using namespace std;


#define CMD_LEN 30 //max len of command buffer

class cmdparser{

public:
  void parse(const char* str);

  cmdparser();
  ~cmdparser();
  Track* track; //track object, should be private
  fileoperations* fop;

//private
  vector<string> split(const string & s);
  bool isTrackCMD(const vector<string> & v);
  bool isUnTrackCMD(const vector<string> & v);
  void trackParse(const vector<string> & v);
  void untrackParse(const vector<string> & v);
  void initTrack(void);
  void setFilename(string & filename, const string & userPath);


private:

  string show(const string & path);
  void printvect(const vector<string> & v);
  bool isshowcmd(const vector<string> & v);
  bool isoneleveldeep(const string & path, const string & abs);

};




#endif
