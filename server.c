#include <unistd.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <limits.h>
#include <string.h>
#include <sys/un.h>
#include "connection.h"
#include "cmdparser.h"

#define MAX_EVENTS 1024 /*Max. number of events to process at one go*/
#define LEN_NAME 16 /*Assuming that the length of the filename won't exceed 16 bytes*/
#define EVENT_SIZE  ( sizeof (struct inotify_event) ) /*size of one event*/
#define BUF_LEN     ( MAX_EVENTS * ( EVENT_SIZE + LEN_NAME )) /*buffer to store the data of events*/

/*
* File server.c
*/


int main(int argc, char *argv[])
{

  /* Testing server output via output.txt, since crontab doesn't allow output to console*/
  FILE *fptr;
  struct sockaddr_un name; //Socket-Type
  int down_flag = 0;
  int ret;
  int result;
  int connection_socket;
  int data_socket;
  char buffer[BUFFER_SIZE];
  cmdparser p;
  /*
   * In case the program exited inadvertently on the last run,
   * remove the socket.
   */
  unlink(SOCKET_NAME);
  /* Create local socket. */
  connection_socket = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if (connection_socket == -1) {
      perror("socket");
      exit(EXIT_FAILURE);
  }
  /*
   * For portability clear the whole structure, since some
   * implementations have additional (nonstandard) fields in
   * the structure.
   */
  memset(&name, 0, sizeof(struct sockaddr_un));
  /* Bind socket to socket name. */
  name.sun_family = AF_UNIX;
  strncpy(name.sun_path, SOCKET_NAME, sizeof(name.sun_path) - 1);

  ret = bind(connection_socket, (const struct sockaddr *) &name,
             sizeof(struct sockaddr_un));
  if (ret == -1) {
      perror("bind");
      exit(EXIT_FAILURE);
  }

  /*
   * Prepare for accepting connections. The backlog size is set
   * to 20. So while one request is being processed other requests
   * can be waiting.
   */

  ret = listen(connection_socket, 20);
  if (ret == -1) {
      perror("listen");
      exit(EXIT_FAILURE);
  }


//Pass the socket information to the track
//p.setTrackSocket(name, connection_socket, data_socket);

  /* This is the main loop for handling connections. */
  for (;;) {
      /* Wait for incoming connection. */
      data_socket = accept(connection_socket, NULL, NULL);
      if (data_socket == -1) {
          perror("accept");
          exit(EXIT_FAILURE);
      }
      else
      {
        printf("Connection success!! \n");
      }

      result = 0;
      for(;;) {
           /* Wait for next data packet. */
           //clear
           buffer[0] = '\0';
           ret = read(data_socket, buffer, BUFFER_SIZE);
           if (ret == -1) {
             perror("read");
             exit(EXIT_FAILURE);
           }
           else
           {

             if(strncmp(buffer, "#END#", BUFFER_SIZE) == 0){
               fptr = fopen("out.txt","a");
               if(fptr == NULL){
                 printf("Error!");
                 exit(1);
               }
               fprintf(fptr,"%s",buffer);
               fprintf(fptr,"\n");
               fclose(fptr);
               printf("SERVER.C IN : %s\n",buffer);

               printf("Got end\n");
               break;
             }
           }
          /* Ensure buffer is 0-terminated. */
          buffer[BUFFER_SIZE - 1] = 0;
          //printf("SERVER.C GOT:%s\n",buffer);
          /* Handle commands. */
          if (strncmp(buffer, "#DOWN#", BUFFER_SIZE) == 0) {
              printf("Got down flag\n");
              down_flag = 1;
              break;
          }
          else{
            //parse command
            printf("parsing\n");
            buffer[BUFFER_SIZE - 1] = 0;
            p.parse(buffer);
            break;
          }
          /* Add received summand. */
          result += atoi(buffer);
      }

      /* Send result. */
      sprintf(buffer, "%d", result);
      ret = write(data_socket, buffer, BUFFER_SIZE);

      if (ret == -1) {
          perror("write");
          exit(EXIT_FAILURE);
      }
      /* Close socket. */
      close(data_socket);

      /* Quit on DOWN command. */
      if (down_flag) {
          break;
      }
  }
  close(connection_socket);

  /* Unlink the socket. */
  unlink(SOCKET_NAME);
  exit(EXIT_SUCCESS);


}
