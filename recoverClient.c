#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include "connection.h"
#include <string>
#include <iostream>


int main(int argc, char *argv[])
{
  std::string out;
  char toTrack[64];

printf("Your file %s has been deleted, would you like to recover it? 1 = YES, 0 = NO\n",argv[1]);
  fseek(stdin,0,SEEK_END);
   scanf("%s",toTrack);

    if(strcmp(toTrack,"0")==0){
      printf("Are you sure? I'll ask once more...\nYour file %s has been deleted, would you like to recover it? 1 = YES, 0 = NO\n",argv[1]);
      scanf("%s",toTrack);
      }


   struct sockaddr_un addr;
   int i;
   int ret;
   int data_socket;
   char buffer[BUFFER_SIZE];

   /* Create local socket. */

   data_socket = socket(AF_UNIX, SOCK_SEQPACKET, 0);
   if (data_socket == -1) {
       perror("socket");
       exit(EXIT_FAILURE);
   }

   /*
    * For portability clear the whole structure, since some
    * implementations have additional (nonstandard) fields in
    * the structure.
    */
   memset(&addr, 0, sizeof(struct sockaddr_un));

   /* Connect socket to socket address */

   addr.sun_family = AF_UNIX;

   strncpy(addr.sun_path, SOCKET_NAME_2, sizeof(addr.sun_path) - 1);

   ret = connect (data_socket, (const struct sockaddr *) &addr,
                  sizeof(struct sockaddr_un));
   if (ret == -1) {
       fprintf(stderr, "The server is down.\n");
       exit(EXIT_FAILURE);
   }

     if (strcmp(toTrack,"0")!=0) {ret = write(data_socket, "yes\0", 4);printf("Saving File...\n");}
     else {ret = write(data_socket, "no\0", 4);printf("File Removed\n");}
     printf("Exiting...\n");
     sleep(1);

     close(data_socket);

     exit(EXIT_SUCCESS);
     return 0;
   }
